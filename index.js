const axios = require('axios').default;
const fs = require('fs');
var throttledqueue = require('throttled-queue');

axios.defaults.timeout = 250000;

const baseUrl = 'https://infinite-castles.herokuapp.com';


var throttle = throttledqueue(100, 1000);
var errorChests = [];
var errorRooms = [];
var full_chests = 0;
var visited = []
var nextNodes = [];



/** OpenChest
 * Parameters : chest(A chest path)
 * Returns: Nothing
 * Workflow: Fetch the content of the chest given in parameter. If the chest contains something, write its ID in the 'chest' file. In all cases, add the chest to the 'visited' list.
 * Error Workflow: If a chest path returns an error, add its path to the 'errorChests' liost in order to retry it later
 */


function openChest(chest) {
  throttle(function () {
    axios.get(baseUrl + chest).then(content => {
        if (errorChests.includes(chest)) {
          errorChests.splice(errorChests.indexOf(chest), 1);
        }
        if (!String(content.data.status).includes('This chest is empty')) {
          fs.appendFile('chests', full_chests + " - " + content.data.id + ' - ' + content.data.status + '\n', function (err) {
            if (err) {
              console.log(err);
            }
          });
          full_chests += 1;
          console.log('Full chests found : ' + full_chests);
        } else {
          if (String(content.data.status).includes('This chest is empty')) {
            console.log('empty');
          }
          visited.push(chest);
          console.log(visited.length + ' nodes visited');
        }
      })
      .catch(err => {
        /* Si le chest n'est pas déjà dans la liste des chests en erreur, on l'ajoute et on log cette erreur dans le fichier correspondant */
        if (err.request._options !== undefined) {
          path = err.request._options.path
          console.log(path);
          if (!errorChests.includes(path)) {
            errorChests.push(path);
            fs.appendFile('errors', path + ' - ' + err.errno + '\n', function (err) {
              if (err) {
                console.log(err);
              }
            });
          }
          console.log(errorChests.length + ' chests still in error');
        } else {
          console.log(err);
        }
      })
  })

}

/**  openRoom
 *  Parameter : roomUrl (A room path)
 *  Returns : Nothing
 *  Workflow step 1 : Fetch the content of the room given in parameter. In all cases, add it to the 'visited' list.  Concatenate the current room's data (Adjacent rooms + Chests) and append all non-visited paths to the 'nextNodes' list for further treatment.
 *  Workflow step 2 : Pop the nextNodes list in order to get the next unseen element to investigate, if it exists. If its a room, recursive call to openRoom ensure continuity, otherwise call openChest() to get the chest's data
 *  Error Workflow : If a room returns an error, add it to the 'errorRooms' list. On each openRoom() call, if one or both of the error Lists contain elements, try to resolve them.
*/
async function openRoom(roomUrl) {
  try {
    const response = await axios.get(baseUrl + roomUrl);
    visited.push(roomUrl);
    var neighbor_rooms = response.data.rooms;
    var neighbor_chests = response.data.chests;
    var neighbors = neighbor_rooms.concat(neighbor_chests);
    neighbors.forEach(neigb => {
      if (visited.includes(neigb)){
        neighbors.splice(0,1, neigb);
      }
    });
    nextNodes = nextNodes.concat(neighbors);
    console.log(nextNodes.length + ' nodes to go');
    var nextNode = nextNodes.shift()

    while (visited.includes(nextNode)) {
      nextNode = nextNodes.shift();
    }

    while(errorRooms.length > 0){
      await(openRoom(errorRooms.shift()))
    }

    while(errorChests.length > 0){
      await(openRoom(errorChests.shift()))
    }

    while (nextNodes.length > 0) {
      if (String(nextNode).includes('rooms')) {
        await openRoom(nextNode);
      } else if (String(nextNode).includes('chests')) {
        openChest(nextNode);
      }
      nextNode = nextNodes.shift();
    }


  } catch (err) {
    if (err.request !== undefined && err.request._options !== undefined) {
      currentRoom = err.request._options.path;
      errorRooms.push(currentRoom);
      fs.appendFile('errors', currentRoom + ' - ' + err.errno + '\n', function (err) {
        if (err) {
          console.log(err);
        }
      });
      console.log(errorRooms.length + ' rooms still in error');
    } else {
      console.log(err);
    }
  }
}

openRoom('/castles/1/rooms/entry');