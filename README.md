# Castles Technical Test

## Fonctionnement
* Installation des dépendances : `yarn install`
* Démarrer le script : `yarn start`
* 
## Choix des technos et des libs

* Techno de dev : Node.js (12.14) (Plus d'aisance sur le langage, déport de la complexité sur le throttling plutot que l'optimisation)
* Libs utilisée :
  *  Axios - Requêtes HTTP (Intégration natives des promises qui sont quand même plus agréables que des callback à la chaine)
  * FS - Pour l'écriture dans un filesystem
  * Throttled-queue : Limiter le nombre d'appels / sec à l'API 

### Itération 1 : Bruteforce Réseau
### Raisonnement

* La structure de l'api est faite de telle manière qu'elle est comparable à un graphe
  * Les chests étant des noeuds terminaux
  * Les rooms sont des noeuds intermédiaires
* Utilisation d'un algo de parcours largeur


### Problèmes rencontrés

* Récupérer le résultat attendu seulement quand la fonction principale est terminée
  * Solution : Ecrire au fil de l'eau dans un fichier de sortie plutot que d'attendre la fin du traitement
* Beaucoup de endpoints renvoient un `ENOTFOUND` en traitement alors qu'ils sont atteignables dans le browser
* Limite de file descriptors concurrents en local + Conncetion Reset côté API : Mettre en place un mécanisme de throttling


## Itération 2 : Throttling 
### Raisonnement
  * Chaque appel récursif est placé dans une throttle-queue pour à la fois:
    * Ne pas surcharger le nombre de sockets concurrentes possible
    * Ne pas se faire jeter par l'API avec un trop grand nombre de calls/sec

### Problèmes rencontrés
* Toujours le même problème de `ENOTFOUND`


## Itération 3 : Gestion d'erreur

### Problèmes rencontrés :
  * Les appels récursifs sur openRoom() ne sont pas compatibles avec le throttling qui fonctionne dans un contexte local 
    * Solution : Utiliser des fonctions async await pour gérer la dépendance des appels récursifs
### Raisonnement
* Les ENOTFOUND des chest/rooms sont sporadiques et peuvent donc être backup avec une récupération d'erreur
  * Solution : Les `ENOTFOUND` sont causés par la limite de file Descriptors concurrents de Node => Réduction du nombre de calls API / sec
  * Si l'ouverture d'un path provoque une erreur, on le met dans une liste
  * Sporadiquement, on retentera l'ouverture de ce path et le renvoi d'une des valeurs nominale provoquera son retrait de la liste d'erreurs

### Améliorations possibles
* Une optimisation du temps de traitement (4645 sec en première passe) doit être pouvoir réalisée


